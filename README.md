# Simulasi Lung disease

Simulasi program untuk skripsi CXR Lung Disease 

## Instalation
- Buat envirotment terlebih dahulu kemudian install seluruh modul dan fungsi yang ada pada file **requirements.txt**.
Setelah clone seluruh repository ini, didalam folder silahkan buat envirotment dengan cara :

```
python3 -m venv env
```

Semudian aktifkan environment dengan cara mengetik dan menjalan perimtah didalam bash command

```
source env/bin/activated
```
Kemudian install seluruh fungsi dan modul dengan menjalankan perintah didalam bash command
```
pip install -r requirements.txt
```
Setelah berhasil melakukan tahapan diatas kemudian bisa melanjutkan ketahap demo program.

## Demo Program
Untuk menjalankan demo program ini dapat memilih model yang ingin digunakan, model yang digunakan terdapat dua jenis yakni model undersampling dan oversampling, untuk mengganti model dapat merubah bagian dibawah ini: 
<img width="1000" alt="teaser" src="./figures/edited_path.png">
Pada gambar diatas dapat memilih dengan cara mencomment salah satu model diatas.

## Ouput Hasil
Setelah melakukan perintah :
```
python simulasi.py
```
dan kemudian select an image akan menghasilkan output seperti dibawah ini.
<img width="1000" alt="teaser" src="./figures/example_run.png">