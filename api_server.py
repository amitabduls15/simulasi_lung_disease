# -*- coding: utf-8 -*-

import torch
from torchvision import transforms
from torchsummary import summary
from PIL import Image
from model import CNN

from matplotlib import pyplot as plt

from sklearn import neighbors, tree
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.model_selection import GridSearchCV
from sklearn.datasets.base import Bunch

import numpy as np
import pickle
# from tqdm import tqdm

import warnings
warnings.filterwarnings('ignore')

########################## Sanic##################################
# Sanic
import asyncio
import uvloop
from sanic import Sanic
from sanic import response
from sanic.response import json
from sanic.request import RequestParameters
from sanic.response import text
from sanic.exceptions import ServerError
from sanic_openapi import swagger_blueprint#, openapi_blueprint
from sanic_openapi import doc
from sanic_cors import CORS, cross_origin

# STD LIB
import base64
import gc
import time
import sys
from datetime import datetime
import argparse

# Configuration
##API
parser = argparse.ArgumentParser(description='TBC, PNEUMONIA DETECTION')
parser.add_argument('--port', type=int, default=8000, help='port')

args = parser.parse_args()

print(">> Launching RONTIA ENGINE")
print(">> Coded by: Abdul Hamid")
boottime1 = time.time()

port = args.port
    

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
app = Sanic()
cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config["CORS_AUTOMATIC_OPTIONS"] = True
# app.blueprint(openapi_blueprint)
app.blueprint(swagger_blueprint)
app.config.API_VERSION = "0.0.0"
app.config.API_TITLE= "RONTIA API SERVER: " , port
app.config.API_TERMS_OF_SERVICE= "null"
app.config.API_CONTACT_EMAIL="amitabduls@gmail.com"

# Data Model untuk Swagger
class rontb64:
    b64img = str


import time

from utils import load_input_image, predict_per_img_tf_api, normalize_tensor,loadImage_API,logging

cuda = True if torch.cuda.is_available() else False

model = CNN()

if cuda:
    model.cuda()
    print('Run With CUDA ^_^')


Target_names = ['Pneumonia','Normal',"Tuberculosis"]

img_load_dset = Bunch()

resume_weights = 'oversampling_best_loss/CNN_checkpoint_ov.pth.tar'
#resume_weights = 'undersampling_best_loss/CNN_checkpoint_ud.pth.tar'

if cuda:
    checkpoint = torch.load(resume_weights)
else:
    # Load GPU model on CPU
    checkpoint = torch.load(resume_weights,
                            map_location=lambda storage,
                            loc: storage)

start_epoch = checkpoint['epoch']
best_loss = checkpoint['best_loss']
model.load_state_dict(checkpoint['cnn'])
logging("=> loaded checkpoint '{}' (trained for {} epochs)".format(resume_weights, 
                                                                checkpoint['epoch']))
softmax = torch.nn.Softmax()

summary(model, (1, 32, 32))

@app.route("/api/rontb64/", methods=["POST", "OPTIONS"])
@doc.summary("Synchronous RONTGEN with Base64 payload")
@doc.consumes(rontb64, location="body")
async def sync_rontb64(request):
    try:
        body = request.json
    except OSError as er:
        raise ServerError("Not valid JSON", status_code=400)
    try:
        starting = time.time()

        b64img = body.get('b64img')
        imgdata = base64.b64decode(b64img) 
        #teksdarib64 = base64.b64decode(b64img).decode("utf-8")
        #print(teksdarib64)
        #words = teksdarib64.split()
        # print("Predicting Progres")
        hasil,label = predict_per_img_tf_api(model,imgdata,cuda=cuda)
        # print(hasil,label)
        # print(type(hasil))
        ending = time.time()
        # print("\nTime Predict end to end {}\n".format(ending-starting))
        result = {"prob_result": np.around(hasil, 2) , "label":label ,"TIME":ending-starting}
        logging("hasil {:.2f}% :{} dalam waktu {}".format(hasil,label, result['TIME']))
        return response.json(result)
    except OSError as er:
        raise ServerError("OSError occured", status_code=500)

boottime2 = time.time()
boottime = boottime2 - boottime1
print(">> RONTIA V0 Engine boot time: ", boottime, " seconds")
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=port, workers=1)
