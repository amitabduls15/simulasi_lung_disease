import torch.nn as nn
import torchvision.transforms as transforms
import torchvision
from torch.autograd import Variable

class CNN(nn.Module):
    def __init__(self):
        super(CNN,self).__init__()
        
        self.cnn1 = nn.Conv2d(in_channels=1, out_channels=16, kernel_size=2,stride=2, padding=0)
        #self.batchnorm1 = nn.BatchNorm2d(16)        #Batch normalization
        #self.tanh = nn.Tanh()
        self.relu = nn.ReLU()                 #RELU Activation
        self.maxpool1 = nn.MaxPool2d(kernel_size=2)   #Maxpooling reduces the size by kernel size. 32/2 = 16
        
        self.cnn2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=2, stride=2, padding=0)
        #self.batchnorm2 = nn.BatchNorm2d(32)
        #self.leakyrelu = nn.LeakyReLU(0.2)
        #self.tanh = nn.Tanh()
        self.relu = nn.ReLU()                 #RELU Activation
        self.maxpool2 = nn.MaxPool2d(kernel_size=2)    #Size now is 32/2 = 16
        
        self.cnn3 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=2, stride=2, padding=0)
        #self.batchnorm3 = nn.BatchNorm2d(64)
        #self.tanh = nn.Tanh()
        self.relu = nn.ReLU()                 #RELU Activation
        
        #Flatten the feature maps. You have 32 feature mapsfrom cnn2. Each of the feature is of size 16x16 --> 32*16*16 = 8192
        self.fc1 = nn.Linear(in_features=64, out_features=3)   #Flattened image is fed into linear NN and reduced to half size
        #self.tanh = nn.Tanh()
        #self.sigmoid = nn.Sigmoid()
        #self.hardtanh = nn.Hardtanh()

        
       
        
    def forward(self,x):
        out = self.cnn1(x)
        out = self.relu(out)
#         out = self.tanh(out)
        out = self.maxpool1(out)
        
        out = self.cnn2(out)
        out = self.relu(out)
#         out = self.tanh(out)
        out = self.maxpool2(out)
        
        out = self.cnn3(out)
        out = self.relu(out)
#         out = self.tanh(out)
        out = out.view(-1,64)   #-1 will automatically update the batchsize as 100; 8192 flattens 32,16,16
        #Then we forward through our fully connected layer 
        out = self.fc1(out)
#         out = self.tanh(out)
        return out
