import torch
from model import CNN

import cv2 as cv
from matplotlib import pyplot as plt

from sklearn import neighbors, tree
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.model_selection import GridSearchCV
from sklearn.datasets.base import Bunch

import numpy as np
import pickle
# from tqdm import tqdm

import warnings
warnings.filterwarnings('ignore')

import time

from tkinter import *
from PIL import Image
from PIL import ImageTk
import tkinter.filedialog as tkFileDialog


model = CNN()

Tensor = torch.FloatTensor
Tensor2 = torch.LongTensor

Target_names = ['Pneumonia','Normal',"Tuberculosis"]

img_load_dset = Bunch()
#img_load_dset['data'] = np.load('/home/abdul/Image-Classification Skripsi/DATA/skripsi_data.npy', allow_pickle=True)
#img_load_dset['images'] = np.load('/home/abdul/Image-Classification Skripsi/DATA/Grayscale 32/skripsi_images.npy', allow_pickle=True)
#img_load_dset['target'] = np.load('/home/abdul/Image-Classification Skripsi/DATA/Grayscale 32/skripsi_target.npy', allow_pickle=True)
#img_load_dset['target_names'] = np.load('/home/abdul/Image-Classification Skripsi/DATA/Grayscale 32/skripsi_target_names.npy', allow_pickle=True)

#stdv = img_load_dset['images'].std()
stdv = 0.23344320219523668
print(stdv)
#mean = img_load_dset['images'].mean()
mean = 0.4901252629326692
print(mean)

def normalize_tensor(tensor,mean=mean,std=stdv,Tensor = False):
    if Tensor==False:
        return((tensor-mean)/std)
    else:
        return((tensor-tensor.mean())/tensor.std())

resume_weights = 'oversampling_best_loss/CNN_checkpoint_ov.pth.tar'
# resume_weights = 'undersampling_best_loss/CNN_checkpoint_ud.pth.tar'

checkpoint = torch.load(resume_weights,
                        map_location=lambda storage,
                        loc: storage)
start_epoch = checkpoint['epoch']
best_loss = checkpoint['best_loss']
model.load_state_dict(checkpoint['cnn'])
print("=> loaded checkpoint '{}' (trained for {} epochs)".format(resume_weights, 
                                                                checkpoint['epoch']))
softmax = torch.nn.Softmax()

from torchvision import transforms
from PIL import Image

def show_image(img_path):
    img = cv.imread(img_path)
    image = Image.open(img_path)
    prediction_transform = transforms.Compose([transforms.Resize([360,360]),
                                      transforms.Grayscale(3),
                                      transforms.ToTensor()]
                                      )
    resizee = transforms.Compose([transforms.Resize([360,360])
                                      ]
                                      )
    rezz = resizee(image)
    # discard the transparent, alpha channel (that's the :3) and add the batch dimension
    image = prediction_transform(image)[:3,:,:].unsqueeze(0)
    image = normalize_tensor(image)
    
    
    
    return rezz, transforms.ToPILImage()(image[0])

def load_input_image(img_path):  
    img = cv.imread(img_path)
    #plt.imshow(img)
    #plt.title('Original')
    #plt.show()
    
    start = time.time()
    image = Image.open(img_path)
    prediction_transform = transforms.Compose([transforms.Resize([32,32]),
                                      transforms.Grayscale(1),
                                      transforms.ToTensor()]
                                      )

    # discard the transparent, alpha channel (that's the :3) and add the batch dimension
    image = prediction_transform(image)[:3,:,:].unsqueeze(0)
    image = normalize_tensor(image)
    
    #plt.imshow(transforms.ToPILImage()(image[0]))
    #plt.title('After Preprocessing')
    #plt.show()
    
    end = time.time()
    print('Prepocessing done with {0:.2f} detik\n'.format(end-start))
    
    return end-start , image, transforms.ToPILImage()(image[0]), img

def predict_per_img_tf(model,file,label):
    print('Starting predict image {}'.format(label))
    waktu,img_tst,_,_ = load_input_image(file)
    strt = time.time()
    model.eval()
    result_train = model(img_tst)
    softmx = softmax(result_train)
    print(softmx)
    values, labels = torch.max(softmx, 1)
    ends = time.time()
    
    print('Predict done with {0:.2f} detik\n'.format(ends-strt))
    
    print('TIME {0:.2f} detik\n'.format(waktu+ends-strt))
    
    if Target_names[labels]==label:
        print('Model benar prediksi dengan tingkat keyakinan {0:.2f}% bahwa pasien terdiagnosis menderita {1} '.format(values[0]*100,label))
        return values[0]*100,Target_names[labels]
    else:
        
        print('Model salah prediksi bahwa sebenarnya dengan tingkat keyakinan {0:.2f}% pasien terdiagnosis menderita {1} '.format(values[0]*100,Target_names[labels]))
        return values[0]*100, Target_names[labels]

# Importing Image and ImageFont, ImageDraw module from PIL package  
from PIL import Image, ImageFont, ImageDraw  
      
# creating a image object  
def img_predict(file,valu,target):
    _,image = show_image(file)

    draw = ImageDraw.Draw(image)  

    # specified font size 
    font = ImageFont.truetype(r'Arial.ttf', 20)  

    text = '{0:.2f}% {1}'.format(valu,target)

    # drawing text size 
    draw.text((5, 5), text, fill ="red", font = font, align ="right") 
    return image

# end-start , image, transforms.ToPILImage()(image[0]), img
# import the necessary packages



def select_image():
    # grab a reference to the image panels
    global panelA, panelB

    # open a file chooser dialog and allow the user to select an input
    # image
    path = tkFileDialog.askopenfilename()

    # ensure a file path was selected
    if len(path) > 0:
        image,edged = show_image(path)
        valu,target = predict_per_img_tf(model,path,'TBC')
        edged = img_predict(path,valu,target)
        # ...and then to ImageTk format
        image = ImageTk.PhotoImage(image)
        edged = ImageTk.PhotoImage(edged)

        # if the panels are None, initialize them
        if panelA is None or panelB is None:
            # the first panel will store our original image
            panelA = Label(image=image)
            panelA.image = image
            panelA.pack(side="left", padx=10, pady=10)

            # while the second panel will store the edge map
            panelB = Label(image=edged)
            panelB.image = edged
            panelB.pack(side="right", padx=10, pady=10)

        # otherwise, update the image panels
        else:
            # update the pannels
            panelA.configure(image=image)
            panelB.configure(image=edged)
            panelA.image = image
            panelB.image = edged

        messagebox.showinfo( "Result", "This image is predicted as {0} with a confidence level of {1:.2f}".format(target, valu))
        # L1 = Label(root, text = "Gambar diatas diatas di prediksi sebagai {} dengan tingkat keyakinan sebesar {}".format(target, valu))
        # L1.pack( side = BOTTOM)
        
# initialize the window toolkit along with the two image panels
root = Tk()
root.title("Welcome to Predicting CXR app")
Label(root, text="Select the CXR image and start predicting pneumonia or normal lung disease",font=("Arial Bold", 50))
# lbl.grid(column=0, row=0)
root.geometry('800x500')
panelA = None
panelB = None

# create a button, then when pressed, will trigger a file chooser
# dialog and allow the user to select an input image; then add the
# button the GUI
from tkinter import messagebox
def helloCallBack():
   messagebox.showinfo( "About APP", "This application was created using a CNN-based mechine learning algorithm to be able to predict lung disease from CXR images")

def helloCallBack_tutorial():
   messagebox.showinfo( "Tutorial APP", "To be able to use this application, please select an image by pressing the select an image button at the bottom")
def helloCallBack_author():
   messagebox.showinfo( "Author","This program was created by Abdul Hamid, NIM 11150940000006, mathematics, UIN Syarif Hidayatullah Jakarta")
   
btn0 = Button(root, text ="About", command = helloCallBack)
btn0.pack(side="top", fill="both", expand="no", padx="10", pady="10")
btn1 = Button(root, text ="Help", command = helloCallBack_tutorial)
btn1.pack(side="top", fill="both", expand="no", padx="10", pady="10")
btn2 = Button(root, text ="Author", command = helloCallBack_author)
btn2.pack(side="top", fill="both", expand="no", padx="10", pady="10")
btn = Button(root, text="Select an image", command=select_image)
btn.pack(side="bottom", fill="both", expand="no", padx="10", pady="10")

# kick off the GUI
root.mainloop()
