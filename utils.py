import cv2 as cv
import numpy as np

import torch
from torchvision import transforms
import time
from PIL import Image
from datetime import datetime
from torch.autograd import Variable


def loadImage_API(img_file):
    img = cv.imdecode(np.fromstring(img_file, np.uint8), cv.IMREAD_COLOR)
    # img = io.imread(img_file)           # RGB order
    if img.shape[0] == 2: img = img[0]
    if len(img.shape) == 2 : img = cv.cvtColor(img, cv.COLOR_GRAY2RGB)
    if img.shape[2] == 4:   img = img[:,:,:3]
    img = np.array(img)
    im = Image.fromarray(img)
    # print("shape when load image {}".format(img.shape))

    return im


def normalize_tensor(tensor,std= 0.23344320219523668,mean = 0.4901252629326692,Tensor = False):
    if Tensor==False:
        return((tensor-mean)/std)
    else:
        return((tensor-tensor.mean())/tensor.std())

def load_input_image(img_path,cuda):  
    image = loadImage_API(img_path)
    #plt.imshow(img)
    #plt.title('Original')
    #plt.show()
    
    

    start = time.time()
    # image = Image.open(img_path)
    prediction_transform = transforms.Compose([transforms.Resize([32,32]),
                                      transforms.Grayscale(1),
                                      transforms.ToTensor()]
                                      )

    # discard the transparent, alpha channel (that's the :3) and add the batch dimension
    image = prediction_transform(image)[:3,:,:].unsqueeze(0)
    
    image = normalize_tensor(image)

    if cuda:
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        image = image.to(device)
        # print(type(image))

    #plt.imshow(transforms.ToPILImage()(image[0]))
    #plt.title('After Preprocessing')
    #plt.show()
    
    end = time.time()
    # print('Prepocessing done with {0:.2f} detik\n'.format(end-start))
    
    return end-start , image

def predict_per_img_tf_api(model,file,cuda):
    Target_names = ['Pneumonia','Normal',"Tuberculosis"]
    softmax = torch.nn.Softmax()
    waktu,img_tst= load_input_image(file,cuda)
    # print(img_tst.shape, type(img_tst))
    # print(Tensor(img_tst))
    if cuda:
        img_tst_ = Variable(img_tst, requires_grad=False)
        model.eval()
        result_train = model(img_tst_)
        softmx = softmax(result_train.cpu())
        # print(softmx)
        values, labels = torch.max(softmx, 1)
        
        return (values.detach().numpy())[0]*100 , Target_names[labels]
    else:
        model.eval()
        result_train = model(img_tst)
        softmx = softmax(result_train)
        # print(softmx)
        values, labels = torch.max(softmx, 1)
        
        return (values.detach().numpy())[0]*100 , Target_names[labels]



def logging(message):
    message_format = "[{}] [INFO]-{}".format(datetime.now(), message)
    print(message_format)